# Learning Ruby

## Style Guides

- [Ruby](https://rubystyle.guide/)
- [Rails](https://rails.rubystyle.guide/)
- [RSpec](https://rspec.rubystyle.guide/)

Enforced with [RuboCop](https://github.com/rubocop/rubocop).


## Online Playground

https://try.ruby-lang.org/playground/

## Debuggers 

- Byebug
- Pry 

## Documenation 

- Ruby interactive: `ri`
- [Official Documention](https://docs.ruby-lang.org/)
